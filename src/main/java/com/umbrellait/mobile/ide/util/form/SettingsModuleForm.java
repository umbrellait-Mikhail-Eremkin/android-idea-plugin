package com.umbrellait.mobile.ide.util.form;

import com.intellij.ide.util.projectWizard.WizardContext;
import com.intellij.openapi.ui.ComponentValidator;
import com.intellij.openapi.ui.ValidationInfo;
import com.intellij.ui.DocumentAdapter;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.text.JTextComponent;

public class SettingsModuleForm {
    private JTextField packageName;
    private JPanel mainPanel;
    private JLabel label2;
    private JLabel mainLabel;
    private JLabel ic_logo;
    private JLabel empty;

    private final ComponentValidator packageNameValidator;

    public SettingsModuleForm(WizardContext context) {
        packageNameValidator = new ComponentValidator(context.getDisposable())
                .withValidator(this::modulePackageValidationInfo)
                .installOn(packageName);

        registerOnDocumentListener(packageName);
    }

    public JComponent getComponent() {
        return mainPanel;
    }

    public String getPackageName() {
        return packageName.getText();
    }

    public boolean validate()  {
        return packageNameValidator.getValidationInfo() == null;
    }

    private ValidationInfo modulePackageValidationInfo() {
        String name = packageName.getText();
        if (name.isBlank()) {
            return new ValidationInfo("Module package can not be empty", packageName);
        }
        if (!name.matches("([a-z]+\\.)+[a-z]+")) {
            return new ValidationInfo(
                    "Should be in java package format 'com.umbrellait.example'",
                    packageName
            );
        }
        return null;
    }

    private void registerOnDocumentListener(@NotNull JTextComponent textComponent) {
        textComponent.getDocument().addDocumentListener(new DocumentAdapter() {
            @Override
            protected void textChanged(@NotNull DocumentEvent e) {
                ComponentValidator.getInstance(textComponent).ifPresent(ComponentValidator::revalidate);
            }
        });
    }
}

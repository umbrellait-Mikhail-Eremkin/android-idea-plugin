package com.umbrellait.mobile.ide.util.project

import com.intellij.openapi.module.ModuleType
import com.intellij.openapi.module.ModuleTypeManager
import com.umbrellait.mobile.ide.util.resources.UitIcons
import javax.swing.Icon

class UitAndroidModuleType : ModuleType<UitAndroidModuleBuilder>("UIT_ANDROID_MODULE") {

    companion object {
        val INSTANCE = ModuleTypeManager.getInstance().findByID("UIT_ANDROID_MODULE")
    }

    override fun createModuleBuilder(): UitAndroidModuleBuilder  = UitAndroidModuleBuilder()

    override fun getName(): String = "Basic Android Project"

    override fun getDescription(): String = "Basic Android project contains domain, data, presentation, ui_kit and resource modules"

    override fun getNodeIcon(isOpened: Boolean): Icon = UitIcons.UitAndroidProject_16
}
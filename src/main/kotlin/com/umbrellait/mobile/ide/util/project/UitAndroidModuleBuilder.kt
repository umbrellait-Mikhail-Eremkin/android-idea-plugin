package com.umbrellait.mobile.ide.util.project

import com.intellij.ide.projectView.ProjectView
import com.intellij.ide.projectView.impl.ProjectViewPane
import com.intellij.ide.util.projectWizard.ModuleBuilder
import com.intellij.ide.util.projectWizard.ModuleWizardStep
import com.intellij.ide.util.projectWizard.SettingsStep
import com.intellij.ide.util.projectWizard.WizardContext
import com.intellij.openapi.Disposable
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.application.WriteAction
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.module.ModifiableModuleModel
import com.intellij.openapi.module.Module
import com.intellij.openapi.module.ModuleType
import com.intellij.openapi.project.DumbService
import com.intellij.openapi.project.Project
import com.intellij.openapi.roots.ModifiableRootModel
import com.intellij.openapi.ui.Messages
import com.intellij.openapi.util.Disposer
import com.intellij.openapi.vfs.LocalFileSystem
import com.intellij.openapi.vfs.VfsUtil
import com.intellij.openapi.vfs.VirtualFile
import com.umbrellait.mobile.ide.util.resources.ProjectTemplates
import java.io.File

open class UitAndroidModuleBuilder : ModuleBuilder() {

    companion object {

        val LOG = Logger.getInstance(UitAndroidModuleBuilder::class.java)
    }

    private var myStep: UitAndroidModuleWizardStep? = null

    override fun getModuleType(): ModuleType<*> = UitAndroidModuleType.INSTANCE

    override fun setupRootModel(rootModel: ModifiableRootModel) {
        super.setupRootModel(rootModel)
    }

    override fun getCustomOptionsStep(context: WizardContext, parentDisposable: Disposable): ModuleWizardStep? {
        if (!context.isCreatingNewProject) {
            return null
        }
        myStep = UitAndroidModuleWizardStep(context)
        Disposer.register(parentDisposable, myStep!!)
        return myStep
    }

    override fun modifySettingsStep(settingsStep: SettingsStep): ModuleWizardStep? {
        // TODO: подумать позже как перенести все настройки на один экран
        return null
    }


    override fun commitModule(project: Project, model: ModifiableModuleModel?): Module? {
        val basePath = moduleFileDirectory
        if (basePath == null) {
            Messages.showErrorDialog("Module path not set", "Internal Error")
            return null
        }
        val baseDir: VirtualFile? = LocalFileSystem.getInstance().refreshAndFindFileByPath(basePath)
        if (baseDir == null) {
            Messages.showErrorDialog("Unable to determine project directory", "Internal Error")
            return null
        }
        projectSetup(baseDir)
        showProjectInProjectWindow(project)
        return super.commitModule(project, model)
    }

    private fun showProjectInProjectWindow(project: Project) {
        ApplicationManager.getApplication().invokeLater {
            DumbService.getInstance(project).runWhenSmart {
                ApplicationManager.getApplication().invokeLater {
                    val view = ProjectView.getInstance(project) ?: return@invokeLater
                    view.changeView(ProjectViewPane.ID)
                }
            }
        }
    }

    private fun projectSetup(baseDir: VirtualFile) {

        val packageName = myStep?.getPackageName() ?: "com.example"

        WriteAction.runAndWait<Throwable> {
            baseDir.run {
                createChildDirectory(this, "buildSrc").run {
                    createFile(this, "build.gradle.kts", ProjectTemplates.buildSrcGradleKts)
                    mkdirs("src/main/kotlin").run {
                        createFile(this, "Android.kt", ProjectTemplates.buildSrcAndroidKt)
                        createFile(this, "Dependencies.kt", ProjectTemplates.buildSrcDependenciesKt)
                        createFile(this, "Extensions.kt", ProjectTemplates.buildSrcExtensionsKt)
                    }
                }
                createChildDirectory(this, "gradle-templates").run {
                    createFile(this, "android.gradle", ProjectTemplates.gradleAndroid)
                }
                createChildDirectory(this, "domain").run {
                    createFile(this, "build.gradle.kts", ProjectTemplates.domainModuleGradleKts)
                    mkdirs("src/main/java")
                        .mkdirs("${packageName}.domain".replace(".", "/"))
                }
                createChildDirectory(this, "data").run {
                    createFile(this, "build.gradle.kts", ProjectTemplates.dataModuleGradleKts)
                    mkdirs("src/main").run {
                        createFile(
                            this, "AndroidManifest.xml",
                            ProjectTemplates.androidLibManifest.replace("%package%", "${packageName}.data")
                        )
                        createChildDirectory(this, "res")
                        createChildDirectory(this, "java").mkdirs("${packageName}.data".replace(".", "/"))
                    }
                }
                createChildDirectory(this, "presentation").run {
                    createFile(this, "build.gradle.kts", ProjectTemplates.presentationModuleGradleKts)
                    mkdirs("src/main").run {
                        createFile(
                            this, "AndroidManifest.xml",
                            ProjectTemplates.androidLibManifest.replace("%package%", "${packageName}.presentation")
                        )
                        createChildDirectory(this, "res")
                        createChildDirectory(this, "java").mkdirs("${packageName}.presentation".replace(".", "/"))
                    }
                }
                createChildDirectory(this, "ui_kit").run {
                    createFile(this, "build.gradle.kts", ProjectTemplates.uiKitModuleGradleKts)
                    mkdirs("src/main").run {
                        createFile(
                            this, "AndroidManifest.xml",
                            ProjectTemplates.androidLibManifest.replace("%package%", "${packageName}.ui_kit")
                        )
                        createChildDirectory(this, "res")
                        createChildDirectory(this, "java").mkdirs("${packageName}.ui_kit".replace(".", "/"))
                    }
                }
                createChildDirectory(this, "resources").run {
                    createFile(this, "build.gradle.kts", ProjectTemplates.resourceKitModuleGradleKts)
                    mkdirs("src/main").run {
                        createFile(
                            this, "AndroidManifest.xml",
                            ProjectTemplates.androidLibManifest.replace("%package%", "${packageName}.resources")
                        )
                        createChildDirectory(this, "res")
                        createChildDirectory(this, "java").mkdirs("${packageName}.resources".replace(".", "/"))
                    }
                }
                createChildDirectory(this, "app").run {
                    createFile(
                        this, "build.gradle.kts",
                        ProjectTemplates.appKitModuleGradleKts
                            .replace("%package%", "${packageName}.app")
                            .replace("%applicationId%", packageName)
                    )
                    mkdirs("src/main").run {
                        createFile(this, "AndroidManifest.xml", ProjectTemplates.androidAppManifest)
                        createChildDirectory(this, "java").mkdirs("${packageName}.app".replace(".", "/")).run {
                            createFile(
                                this, "App.kt",
                                ProjectTemplates.androidAppKt.replace("%package%", "${packageName}.app")
                            )
                            createFile(
                                this, "MainActivity.kt",
                                ProjectTemplates.androidMainActivityKt.replace("%package%", "${packageName}.app")
                            )
                        }
                        createChildDirectory(this, "res").run {
                            VfsUtil.findFileByURL(ProjectTemplates.androidAppResPath)?.let {
                                VfsUtil.copyDirectory(this, it, this, null)
                            }
                        }
                    }
                }
                createFile(this, "gradle.properties", ProjectTemplates.gradleProperties)
                createFile(this, "build.gradle.kts", ProjectTemplates.projectBuildGradleKts)
                createFile(this, "settings.gradle.kts", ProjectTemplates.settingsBuildGradleKts)
            }
        }
    }

    private fun VirtualFile.createFile(requestor: Any, fileName: String, content: String) {
        createChildData(requestor, fileName).setBinaryContent(content.toByteArray())
    }

    private fun VirtualFile.mkdirs(path: String): VirtualFile =
        VfsUtil.createDirectories(File(this.path, path).absolutePath)
}
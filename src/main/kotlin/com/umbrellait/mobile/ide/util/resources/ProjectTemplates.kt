package com.umbrellait.mobile.ide.util.resources

object ProjectTemplates {

    private fun load(path: String): String = ProjectTemplates::class.java.getResource(path).readText()

    val projectBuildGradleKts = load("/templates/project.build.gradle.kts")
    val settingsBuildGradleKts = load("/templates/settings.build.gradle.kts")

    val buildSrcGradleKts = load("/templates/buildSrc/build.gradle.kts")
    val buildSrcAndroidKt = load("/templates/buildSrc/src/Android.kt.txt")
    val buildSrcDependenciesKt = load("/templates/buildSrc/src/Dependencies.kt.txt")
    val buildSrcExtensionsKt = load("/templates/buildSrc/src/Extensions.kt.txt")

    val gradleProperties = load("/templates/gradle.properties")
    val gradleAndroid = load("/templates/gradle-templates/android.gradle.txt")

    val domainModuleGradleKts = load("/templates/module/domain.build.gradle.ktx.txt")
    val dataModuleGradleKts = load("/templates/module/data.build.gradle.ktx.txt")
    val presentationModuleGradleKts = load("/templates/module/presentation.build.gradle.ktx.txt")
    val uiKitModuleGradleKts = load("/templates/module/ui_kit.build.gradle.ktx.txt")
    val resourceKitModuleGradleKts = load("/templates/module/resource.build.gradle.ktx.txt")
    val appKitModuleGradleKts = load("/templates/module/app.build.gradle.ktx.txt")

    val androidLibManifest = load("/templates/android/lib/AndroidManifest.xml")

    val androidAppManifest = load("/templates/android/app/AndroidManifest.xml")
    val androidAppKt = load("/templates/android/app/App.kt.txt")
    val androidMainActivityKt = load("/templates/android/app/MainActivity.kt.txt")
    val androidAppResPath = ProjectTemplates::class.java.getResource("/templates/android/app/res")!!
}
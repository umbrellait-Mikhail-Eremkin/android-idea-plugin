package com.umbrellait.mobile.ide.util.action

import com.intellij.ide.impl.NewProjectUtil
import com.intellij.ide.projectWizard.NewProjectWizard
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.project.DumbAware
import com.intellij.openapi.roots.ui.configuration.ModulesProvider
import com.intellij.openapi.wm.impl.welcomeScreen.NewWelcomeScreen

class NewProjectAction : AnAction("New Umbrella Android..."), DumbAware {

    override fun update(e: AnActionEvent) {
        if (NewWelcomeScreen.isNewWelcomeScreen(e)) {
            e.presentation.text = "New Umbrella Project"
        }
    }

    override fun actionPerformed(e: AnActionEvent) {
        val wizard = NewProjectWizard(null, ModulesProvider.EMPTY_MODULES_PROVIDER, null)
        NewProjectUtil.createNewProject(wizard)
    }
}
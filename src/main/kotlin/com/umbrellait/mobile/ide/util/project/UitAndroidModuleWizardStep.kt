package com.umbrellait.mobile.ide.util.project

import com.intellij.ide.util.projectWizard.ModuleWizardStep
import com.intellij.ide.util.projectWizard.WizardContext
import com.intellij.openapi.Disposable
import com.umbrellait.mobile.ide.util.form.SettingsModuleForm
import javax.swing.JComponent

class UitAndroidModuleWizardStep(context: WizardContext) : ModuleWizardStep(), Disposable {

    private val settingsModuleForm =
        SettingsModuleForm(context)

    override fun getComponent(): JComponent = settingsModuleForm.component

    override fun updateDataModel() = Unit

    override fun dispose() = Unit

    override fun validate(): Boolean = settingsModuleForm.validate()

    fun getPackageName () : String = settingsModuleForm.packageName
}
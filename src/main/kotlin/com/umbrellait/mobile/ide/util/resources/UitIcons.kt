package com.umbrellait.mobile.ide.util.resources

import com.intellij.openapi.util.IconLoader

object UitIcons {

    private fun load(path: String) = IconLoader.getIcon(path, UitIcons::class.java)

    val UitAndroidProject_16 = load("/icons/UitAndroidProject16.png")
    val UitAndroidProject_32 = load("/icons/UitAndroidProject32.png")

}
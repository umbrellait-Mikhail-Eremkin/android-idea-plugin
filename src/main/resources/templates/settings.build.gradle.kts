rootProject.name = "android-app"

include(":app")
include(":domain")
include(":data")
include(":presentation")
include(":resources")
include(":ui_kit")